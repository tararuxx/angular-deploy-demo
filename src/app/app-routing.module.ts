import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// routing needed for switching between components
// editing the routes list here
import { ShipperCreateComponent } from './shipper-create/shipper-create.component';
import { ShipperEditComponent } from './shipper-edit/shipper-edit.component';
import { ShipperListComponent } from './shipper-list/shipper-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'shipper-list' },
  { path: 'create-shipper', component: ShipperCreateComponent },
  { path: 'shipper-list', component: ShipperListComponent },
  { path: 'shipper-edit/:id', component: ShipperEditComponent },
  // default below
  { path: '**', component: ShipperListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
