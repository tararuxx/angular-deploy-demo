import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';
@Component({
  selector: 'app-shipper-create',
  templateUrl: './shipper-create.component.html',
  styleUrls: ['./shipper-create.component.css']
})
export class ShipperCreateComponent implements OnInit {

  // want to import the following details
  @Input() shipperDetails = { id: 0, companyName: '', phone: ''}
  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }
  ngOnInit(): void {
  }

  // what we need all the injected things for
  addShipper() {
    // using createShipper api call to insert into db
    this.restApi.createShipper(this.shipperDetails).subscribe( (data: {}) => {
      this.router.navigate(['/shipper-list'])
    })
  }
}
