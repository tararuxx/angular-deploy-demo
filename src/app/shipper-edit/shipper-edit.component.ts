import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-shipper-edit',
  templateUrl: './shipper-edit.component.html',
  styleUrls: ['./shipper-edit.component.css']
})
export class ShipperEditComponent implements OnInit {
  // id pulling in the parameters
  id = this.actRoute.snapshot.params['id'];
  shipperDetails: any = {};

  constructor(
    public router: Router,
    public restApi: RestApiService,
    public actRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // want to subscribe
    this.restApi.getShipper(this.id).subscribe((data: {}) => {
      this.shipperDetails = data;
    })
  }

  updateShipper() {
    if(window.confirm('Are you sure you want to update?')) {
      this.restApi.updateShipper(this.id, this.shipperDetails).subscribe(data => {
        this.router.navigate(['/shipper-list'])
      })
    }
  }

}
