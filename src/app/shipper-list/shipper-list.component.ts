import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { Shipper } from '../shared/shipper';

@Component({
  selector: 'app-shipper-list',
  templateUrl: './shipper-list.component.html',
  styleUrls: ['./shipper-list.component.css']
})

export class ShipperListComponent implements OnInit {

  shippers: Shipper[] = [];

  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadShippers()
  }

  loadShippers() {
    return this.restApi.getShippers().subscribe((data: Shipper[]) => {
        this.shippers = data;
    })
  }

  deleteShipper(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteShipper(id).subscribe(data => {
        this.loadShippers()
      })
    }
  }

}
